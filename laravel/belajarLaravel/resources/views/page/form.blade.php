@extends('layouts.master')
@section('title') 
Halaman Form
@endsection
@section('sub-title') 
halaman form
@endsection
@section('content') 
<h2>Buat Account Baru!</h2>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
    @csrf
        <label>First name</label> <br>
        <input type="text" name="fname"> <br> <br>
        <label>Last name</label> <br>
        <input type="text" name="lname"> <br> <br>
        <label>Gender:</label> <br> <br>
        <input type="radio" name="gender" value="1">Male <br>
        <input type="radio" name="gender" value="2">Female <br>
        <input type="radio" name="gender" value="3">Other <br> <br>
        <label>Nationality:</label> <br> <br>
        <select name="nationality">
            <option value="1">Indonesia</option>
            <option value="2">Malaysia</option>
            <option value="3">Singapura</option>
            <option value="4">Australia</option>
        </select> <br> <br>
        <label>Language Spoken:</label> <br> <br>
        <input type="checkbox" name="language spoken" value="1"> Bahasa Indonesia <br> 
        <input type="checkbox" name="language spoken" value="2"> English <br>
        <input type="checkbox" name="language spoken" value="3"> Other <br> <br>
        <label>Bio:</label> <br> <br>
        <textarea cols="30" rows="10"></textarea> <br> 

        <input type="submit" value="Sign Up">
@endsection


    </form>

