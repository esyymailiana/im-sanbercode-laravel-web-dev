@extends('layouts.master')
@section('title') 
Halaman Detail Cast
@endsection
@section('sub-title') 
Cast
@endsection
@section('content')

<h1>{{$cast->nama}}</h1>
<h4>{{$cast->umur}}</h4>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-secondary btn-sn">Kembali</a>
@endsection