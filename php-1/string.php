<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Soal Pertama</h2>
    <?php 
    echo "<h3> Soal No.1</h3>";
    $kalimat1a = "Hello PHP!";
    echo "Kalimat pertama a : ". $kalimat1a . "<br>";
    echo "Panjang String : ". strlen($kalimat1a) . "<br>";
    echo "Jumlah Kata : ". str_word_count($kalimat1a) . "<br><br>";
    $kalimat1b = "I'm ready for the challenges";
    echo "Kalimat pertama b : ". $kalimat1b . "<br>";
    echo "Panjang String : ". strlen($kalimat1b) . "<br>";
    echo "Jumlah Kata : ". str_word_count($kalimat1b) . "<br><br>";

    echo "<h3> Soal No.2</h3>";
    $string2 = "I love PHP";
    echo "Kalimat kedua : " . $string2. "<br>";
    echo "Kata Pertama : " . substr($string2,0,1) . "<br>";
    echo "Kata Kedua : " . substr($string2,2,4) . "<br>";
    echo "Kata Ketiga : " . substr($string2,7,3) . "<br>";

    echo "<h3> Soal No.3</h3>";
    $string3 = "PHP is old but sexy!";
    echo "Kalimat ketiga : ". $string3 . "<br>";
    echo "Ganti Kalimat ketiga : ". str_replace("sexy","awesome",$string3);

    ?>
</body>
</html>
